
const Me = imports.misc.extensionUtils.getCurrentExtension();
const {ItemsSource} = Me.imports.source;
const JsTextFile = Me.imports.file;
const JsTodoTxt = Me.imports.third_party.jsTodoTxt.jsTodoTxt;

var TodoTxtSource = class extends ItemsSource {
    constructor(name, props) {
        super(name, props);
        this.filepath = this.props.filepath;
    }

    load() {
        if (this.items.length === 0) {
            let items = [];
            const file = new JsTextFile.JsTextFile(this.filepath);
            for (let line of file.lines) {
                var todo = new JsTodoTxt.TodoTxtItem(line);
                if (todo) {
                    const context = todo.contexts ? `@[${todo.contexts}] ` : '';
                    const project = todo.projects ? `+[${todo.projects}] ` : '';
                    const title = todo.text.includes(': ') ? todo.text.split(': ')[0] : todo.text;
                    items.push(`${context}${project}${title}`);
                }
            }
            this.items = items;
        }
    }
};
