const TimerState = {
    STOPPED: 'Stopped', PAUSED: 'Paused', RUNNING: 'Running',
};

var Timer = class {
    constructor(label, source) {
        this.state = TimerState.STOPPED;
        this.lastUpdate = 0;
        this.timePassed = 0;
        this.startDate = 0;
        this.label = label;
        this.source = source;
    }

    isRunning() {
        return this.state === TimerState.RUNNING;
    }

    isPaused() {
        return this.state === TimerState.PAUSED;
    }

    start() {
        this.state = TimerState.RUNNING;
        let datenow = new Date();
        this.lastUpdate = datenow.getTime();
        this.startDate = datenow.toISOString();
        this.timePassed = 0;
    }

    pause() {
        this.state = TimerState.PAUSED;
        this.update();
    }

    resume() {
        this.lastUpdate = new Date().getTime();
        this.state = TimerState.RUNNING;
    }

    stop() {
        this.state = TimerState.STOPPED;
        this.lastUpdate = 0;
        this.timePassed = 0;
    }

    update() {
        if (this.state === TimerState.RUNNING) {
            let timeElapsed = (new Date().getTime() - this.lastUpdate) / 1000.0;
            this.timePassed = Math.round(Math.max(this.timePassed + timeElapsed, 0));
            this.lastUpdate = new Date().getTime();

            if (this.timePassed <= 0) {
                this.state = TimerState.STOPPED;
                this.timePassed = 0;
            }
        }
    }

    setTimePassed(timePassed) {
        this.timePassed = timePassed;
    }
};
