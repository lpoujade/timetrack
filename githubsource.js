
const Me = imports.misc.extensionUtils.getCurrentExtension();
const {ItemsSource} = Me.imports.source;
const {Soup} = imports.gi;
const ByteArray = imports.byteArray;

const GITHUB_CQL_API = 'https://api.github.com/graphql';

var GithubSource = class extends ItemsSource {
    constructor(name, props) {
        super(name, props);
        this.url = GITHUB_CQL_API;
        this.username = this.props.username;
        this.token = this.props.token;

        this.session = Soup.Session.new();
    }

    load() {
        if (this.items.length === 0) {
            const _doc = JSON.stringify({
                query: `
query {
  search(first: 100, type: ISSUE, query: "state:open assignee:${this.username}") {
    issueCount
    pageInfo { hasNextPage endCursor }
    edges {
      node {
        ... on Issue {
          title
          number
          repository { name }
        }
      }
    }
  }
}`,
            });

            let request = Soup.Message.new('POST', this.url);
            let reqbody = Uint8Array.from(Array.from(_doc).map(letter => letter.charCodeAt(0)));
            request.set_request_body_from_bytes('application/json', reqbody);
            request.request_headers.append('Accept', 'application/json');
            request.request_headers.append('User-Agent', 'timetrack/lpo.host');
            request.request_headers.append('Authorization', `Bearer ${this.token}`);
            try {
                let r = this.session.send_and_read(request, null);
                let rbody = ByteArray.toString(r.get_data());
                let raw_items = JSON.parse(rbody);
                if (raw_items.data.search.issueCount === 0)
                    this.item = [];
                else
                    this.items = raw_items.data.search.edges.map(e => `[${e.node.repository.name}] #${e.node.number} ${e.node.title}`);
            } catch (e) {
                console.log('catched network exception', e);
            }
        }
        return this.items;
    }
};
