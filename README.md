# Timetrack

***!! Work in progress !!***

A Gnome Shell extension to track your work time between differents items, fetched from multiples sources (currently implemented: [todo.txt](http://todotxt.org/), issues from [Gitlab](https://gitlab.org) and [Github](https://github.com)) and exported as a CSV file.

# Usage

Once the extension is installed, open the preferences and set "Log file path" to the path you want. Then, add sources by clicking on the source type (todotxt, gitlab, github). For each source, you can configure the name, if it is enabled or not, and specific source properties which are:
- for todo.txt: the path of the todo file
- for github: an auth token and a username to search for assigned issues
- for gitlab: an auth token, a username to search for assigned issues, and a gitlab server url


#### auth

For Gitlab and Github, you need to create an auth token and give it read-only access to your repositories and issues.  
In Gitlab you'll find this under User account > Settings > Tokens,  
in Github in User account > Settings > Developer settings > Personnal Access Tokens > Tokens (classic)

> for Github, the recent "Fine-grained tokens" should be usable, but for now I was only able to make it work using classic tokens.

# Debug

Clone this repository, or add a link to it, in `~/.local/share/gnome-shell/extensions`

Under Wayland:

    MUTTER_DEBUG_DUMMY_MODE_SPECS=1366x768 dbus-run-session -- gnome-shell --nested --wayland

Under X11: restart gnome-shell to reload (alt-f2 r)

### settings

Use `glib-compile-schemas schemas/` when changing `schemas/org.gnome.shell.extensions.timetrack.gschema.xml`

Use `dconf-editor /org/gnome/shell/extensions/timetrack` to manually erase settings and use newly compiled schema


---
# License
[MIT](LICENSE)

[License-Url]: http://opensource.org/licenses/MIT
