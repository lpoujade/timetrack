/** extension.js
 * MIT License
 * Copyright © 2023 Aliaksei Zhuk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */


const {Clutter, GObject, St} = imports.gi;
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const MainLoop = imports.mainloop;

// https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/misc/extensionUtils.js
const Me = imports.misc.extensionUtils.getCurrentExtension();
const ExtensionUtils = imports.misc.extensionUtils;
const Misc = Me.imports.misc;
const Timer = Me.imports.timer;
const JsTextFile = Me.imports.file;
const {GithubSource} = Me.imports.githubsource;
const {TodoTxtSource} = Me.imports.todotxtsource;
const {GitlabSource} = Me.imports.gitlabsource;

const UPDATE_FREQ = 1000;

const SOURCES_BY_TYPES = {
    gitlab: GitlabSource,
    todotxt: TodoTxtSource,
    github: GithubSource,
};

let gtimers = {};
let gcurrenttimer = null;
let gmenuitems = {};
let gsubmenuitems = {};
let gsources = {};

const Indicator = GObject.registerClass(class Indicator extends PanelMenu.Button {
    _init() {
        super._init(0.0, 'Toggle Button');
        this.sighandlers = [];

        // runtime properties from previous instance
        this.timers = gtimers;
        this.menuitems = gmenuitems;
        this.submenuitems = gsubmenuitems;
        this.current_timer = gcurrenttimer;
        this.sources = gsources;

        this.filepath = '';
        this.sourcesprops = {};

        this.settings = ExtensionUtils.getSettings();
        this.filepath = this.settings.get_string('time-file-path');
        this.sourcesprops = this.settings.get_value('sources').recursiveUnpack();

        this.setMenu(new PopupMenu.PopupMenu(this, St.Align.START, St.Side.TOP));

        this._initSources();

        this._time = new St.Label({
            text: this.current_timer !== null ? Misc.formatTime(this.timers[this.current_timer].timePassed) : '⏲',
            y_align: Clutter.ActorAlign.CENTER, style_class: 'paused',
        });
        this.add_child(this._time);

        if (this.current_timer !== null) {
            this.timers[this.current_timer].start();
            this.timeout = MainLoop.timeout_add(1000, () => {
                this.timers[this.current_timer].update();
                return true;
            });
        }

        this.sighandlers.push(
            this.settings.connect('changed::time-file-path', () => {
                this.filepath = this.settings.get_string('time-file-path');
                this.saveFile();
            })
        );

        this.sighandlers.push(
            this.settings.connect('changed::sources', () => {
                this.sourcesprops = this.settings.get_value('sources').recursiveUnpack();
                this.sources = {};
                this._initSources();
            })
        );
    }

    _initSources() {
        this.menu.removeAll();
        for (let sourcename of Object.keys(this.sourcesprops)) {
            if (this.sourcesprops[sourcename][0] === false)
                continue;
            const sourcetype = this.sourcesprops[sourcename][1];
            if (this.sources[sourcename] === undefined) {
                this.sources[sourcename] = new SOURCES_BY_TYPES[sourcetype](sourcename, this.sourcesprops[sourcename][2]);
                this.sources[sourcename].load();
            }
            const submenuitem = new PopupMenu.PopupSubMenuMenuItem(this.sources[sourcename].name);

            // reload btn
            var reloadItem = new PopupMenu.PopupMenuItem('↺');
            reloadItem.set_name('reload');
            reloadItem.connect('activate', this._reloadSource.bind(this, sourcename));
            submenuitem.menu.addMenuItem(reloadItem);

            for (let i of this.sources[sourcename].items) {
                if (this.timers[i] === undefined) {
                    let timer = new Timer.Timer(i, sourcename);
                    this.timers[i] = timer;
                }
                var menuItem = new PopupMenu.PopupMenuItem(i);
                this.menuitems[i] = menuItem;
                menuItem.set_name(i);
                menuItem.connect('activate', this._onItemClick.bind(this));
                submenuitem.menu.addMenuItem(menuItem);
                this.submenuitems[i] = submenuitem;

                if (this.current_timer === i) {
                    submenuitem.label.set_style_class_name('bold');
                    menuItem.label.set_style_class_name('bold');
                }
            }
            this.menu.addMenuItem(submenuitem);
        }
    }

    _saveTimerToFile(timer) {
        if (timer.timePassed === 0)
            return;
        let file = new JsTextFile.JsTextFile(this.filepath);
        file.addLine(`${timer.source};${timer.label};${timer.startDate};${timer.timePassed}`);
        file.saveFile();
    }

    // reload a certain source and reset the view
    // other sources uses their cached items
    _reloadSource(srcname) {
        if (this.current_timer)
            this._saveTimerToFile(this.timers[this.current_timer]);
        this.sources[srcname].reload();
        this._initSources();
    }

    _startTimeout(timer) {
        this.timeout = MainLoop.timeout_add(UPDATE_FREQ, () => {
            timer.update();
            this._updateLabel();
            if (timer.timePassed > 3600) {
                this._stopSaveTimer(this.timers[this.current_timer]);
                timer.start();
                this._startTimeout(timer);
            }
            return true;
        });
    }

    _stopSaveTimer(timer) {
        MainLoop.source_remove(this.timeout);
        this._saveTimerToFile(this.timers[timer]);
        this.timers[timer].stop();
        this.menuitems[timer].label.set_style_class_name('normal');
        this.submenuitems[timer].label.set_style_class_name('normal');
        this.timeout = null;
    }

    // when an item is selected, stop previous timer,
    // save previous timer to file, then start/stop
    // selected timer depending of its state
    _onItemClick(actor) {
        const itemid = actor.get_name();
        if (this.current_timer !== null && this.current_timer !== itemid)
            this._stopSaveTimer(this.current_timer);

        actor.label.set_style_class_name('bold');
        this.submenuitems[itemid].label.set_style_class_name('bold');
        this.current_timer = itemid;
        const timer = this.timers[itemid];

        if (timer.isRunning()) {
            this._stopSaveTimer(this.current_timer);
        } else {
            timer.start();
            this._startTimeout(timer);
        }
    }

    _updateLabel() {
        // this._time.set_text(this.current_timer !== null ? Misc.formatTime(this.timers[this.current_timer].timePassed) : '-');
        const label = `${this.timers[this.current_timer].source}|${this.current_timer}`;
        this._time.set_text(this.current_timer !== null ? label : '⏲');
    }

    destroy() {
        if (this.timeout) {
            this._stopSaveTimer(this.current_timer);
            gcurrenttimer = this.current_timer;
            gtimers = this.timers;
            gmenuitems = this.menuitems;
            gsubmenuitems = this.submenuitems;
            gsources = this.sources;
        }
        this.sighandlers.forEach(sh => global.settings.disconnect(sh));
        this.sighandlers = [];
        super.destroy();
    }
});

class Extension {
    constructor(uuid) {
        this._uuid = uuid;
    }

    enable() {
        this._indicator = new Indicator();

        Main.panel.addToStatusArea(this._uuid, this._indicator);
    }

    disable() {
        this._indicator.destroy();
        this._indicator = null;
    }
}

// eslint-disable-next-line no-unused-vars, jsdoc/require-jsdoc
function init(meta) {
    return new Extension(meta.uuid);
}
