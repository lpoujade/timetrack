/* eslint-disable */
// https://gitlab.com/todo.txt-gnome-shell-extension/todo-txt-gnome-shell-extension/-/blob/010df52eed2d91073701970d97176be597556145/libs/utils.js

const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const ByteArray = imports.byteArray;

const Me = imports.misc.extensionUtils.getCurrentExtension();

const LINE_NOT_FOUND = -1;

var fexists = (path) => { return GLib.file_test(path, GLib.FileTest.EXISTS); }

/* exported JsTextFile */
var JsTextFile = class {
    constructor(path) {
        this.path = path;
        this._lines = null;
        this._loadLines();
    }

    // Returns true if file exists, false if not
    exists() {
        if (GLib.file_test(this.path, GLib.FileTest.EXISTS)) {
            return true;
        }
        return false;
    }

    _arrayToString(array) {
        if (array instanceof Uint8Array) {
            return ByteArray.toString(array);
        }
        return array.toString();
    }


    // Loads all lines from the text file
    _loadLines() {
        if (!this.exists()) {
            this._lines = [];
            return;
        }
        const file = Gio.file_new_for_path(this.path);
        const [result, contents] = file.load_contents(null);
        if (!result) {
            console.error('failed to load file content');
        }
        let content = this._arrayToString(contents);
        const LAST_CHARACTER = -1;
        const FIRST_CHARACTER = 0;
        if (content.slice(LAST_CHARACTER) == '\n') {
            content = content.slice(FIRST_CHARACTER, LAST_CHARACTER);
        }
        this._lines = content.split('\n');
    }

    // Returns the number in the lines-array that contains the matching string
    // Returns LINE_NOT_FOUND if text is not found
    _getLineNum(text) {
        if (!this.exists()) {
            return LINE_NOT_FOUND;
        }
        return this._lines.indexOf(text);
    }

    // Saves the lines to a file
    saveFile(removeEmptyLines = true) {
        if (!this.exists()) {
            const file = Gio.file_new_for_path(this.path);
            const state = file.create(Gio.FileCreateFlags.NONE, null);
        }
        if (removeEmptyLines === true) {
            this._removeEmptyLines();
        }
        try {
            const lines = this._lines.join('\n');
            // make sure file ends with a newline
            GLib.file_set_contents(this.path, `${lines}\n`);
        } catch (exception) {
        }
    }

    _removeEmptyLines() {
        this._lines = this._lines.filter((value) => {
            return (value !== '');
        });
    }

    get lines() {
        if (!this.exists()) {
            return [];
        }
        return this._lines;
    }

    removeLine(text) {
        const lineNum = this._getLineNum(text);
        if (lineNum == LINE_NOT_FOUND) {
            return false;
        }
        const NUMBER_OF_LINES_TO_REMOVE = 1;
        this._lines.splice(lineNum, NUMBER_OF_LINES_TO_REMOVE);
        return true;
    }

    addLine(text, atFront = false) {
        if (!this.exists()) {
            return false;
        }
        if (atFront === true) {
            this._lines.unshift(text);
            return true;
        }
        this._lines.push(text);
        return true;
    }

    modifyLine(oldtext, newtext) {
        if (!this.exists()) {
            return false;
        }
        const index = this._getLineNum(oldtext);
        if (index != LINE_NOT_FOUND) {
            this._lines[index] = newtext;
            return true;
        }
        return false;
    }

    set lines(newlines) {
        this._lines = newlines;
    }
};

