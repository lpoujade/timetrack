const {Adw, Gtk, GLib} = imports.gi;
const Me = imports.misc.extensionUtils.getCurrentExtension();
const {JsTextFile, fexists} = Me.imports.file;
const Misc = Me.imports.misc;

const ExtensionUtils = imports.misc.extensionUtils;

const GSETTINGS_FORMAT = 'a{s(bsa{ss})}';

// eslint-disable-next-line jsdoc/require-jsdoc, no-unused-vars
function init() {
}

/**
 *
 * @param {object} sources current sources
 */
function save_sources(sources) {
    const settings = ExtensionUtils.getSettings();
    const gsources = new GLib.Variant(GSETTINGS_FORMAT, sources);
    settings.set_value('sources', gsources);
}

/**
 *
 * @param {object} sources .
 * @param {object} srcname .
 * @param {object} srcdata .
 */
function add_source(sources, srcname, srcdata) {
    if (Object.keys(sources).includes(srcname))
        return false;
    srcdata[0] = false;
    sources[srcname] = srcdata;
    save_sources(sources);
    return true;
}

/**
 *
 * @param {object} sources current sources
 * @param {object} defaultsources default sources
 * @param {Adw.Page} page settings page to add new sources row
 */
function create_new_source_form(sources, defaultsources, page) {
    let group = new Adw.PreferencesGroup({title: 'Click on a type to add a source'});
    let row = new Adw.ActionRow();
    group.add(row);
    const defaults = defaultsources();
    for (let srcname of Object.keys(defaults)) {
        const type = defaults[srcname][1];
        const btn_add = new Gtk.Button({label: type, valign: Gtk.Align.CENTER});
        row.add_suffix(btn_add);
        btn_add.connect('clicked', () => {
            const defaultsrc = defaultsources();
            add_source(sources, srcname, defaultsrc[srcname]);
            page.add(create_source_group(srcname, page, sources));
        });
    }
    return group;
}

/**
 *
 * @param {string} srcname name of the source
 * @param {Adw.PreferencesPage} page where to add the source form
 * @param {object} sources current sources
 */
function create_source_group(srcname, page, sources) {
    const enabled = sources[srcname][0],
        type = sources[srcname][1],
        props = sources[srcname][2];

    const cursourcesgroup = new Adw.PreferencesGroup({title: `[${type}] ${srcname}`});
    let sourcerow = new Adw.ActionRow();
    const nameinput = new Gtk.Entry({placeholder_text: srcname, valign: Gtk.Align.CENTER});
    const toggle = new Gtk.Switch({active: enabled, valign: Gtk.Align.CENTER});
    const btn_delete = new Gtk.Button({label: 'Delete', valign: Gtk.Align.CENTER});

    cursourcesgroup.add(sourcerow);

    sourcerow.add_suffix(nameinput);
    sourcerow.add_suffix(toggle);
    sourcerow.add_suffix(btn_delete);

    // modify source name
    nameinput.connect('activate', entry => {
        const newsrcname = entry.get_buffer().get_text();

        const srcdata = sources[srcname];
        delete sources[srcname];
        add_source(sources, newsrcname, srcdata);

        page.remove(cursourcesgroup);
        page.add(create_source_group(newsrcname, page, sources));
        return true;
    });

    // enable/disable
    toggle.connect('state-set', (sw, state) => {
        sources[srcname][0] = state;
        save_sources(sources);
    });

    // delete
    btn_delete.connect('clicked', () => {
        delete sources[srcname];
        save_sources(sources);
        page.remove(cursourcesgroup);
    });

    // props specific to source
    sourcerow = new Adw.ActionRow();
    cursourcesgroup.add(sourcerow);
    for (const prop of Object.keys(props)) {
        if (prop.length === 0)
            continue;
        let propentry = new Gtk.Entry({placeholder_text: prop, valign: Gtk.Align.CENTER});
        if (props[prop].length > 0)
            propentry.set_text(props[prop]);
        sourcerow.add_suffix(propentry);

        propentry.connect('changed', entry => {
            Misc.debounce(() => {
                sources[srcname][2][prop] = entry.get_buffer().get_text();
                save_sources(sources);
            }, 1000)();
        });
    }

    return cursourcesgroup;
}

/**
 * Build a row containing a title, a text input, and a save Button
 * handle the CSV log file path
 */
function create_logfile_row() {
    const settings = ExtensionUtils.getSettings();
    const logfilepath = settings.get_string('time-file-path');

    const row = new Adw.ActionRow({title: 'Log file path'});
    const file_input = new Gtk.Entry({placeholder_text: 'filepath', valign: Gtk.Align.CENTER});
    const save_btn = new Gtk.Button({valign: Gtk.Align.CENTER});

    update_btn_label(save_btn, logfilepath);
    file_input.set_text(logfilepath);

    file_input.connect('changed', val => Misc.debounce(() => update_btn_label(save_btn, val.get_buffer().get_text()), 1000)());
    save_btn.connect('clicked', () => {
        const path = file_input.get_buffer().get_text();
        settings.set_string('time-file-path', path);
        const f = new JsTextFile(path);
        if (!f.exists())
            f.saveFile();
        update_btn_label(save_btn, path);
    });

    row.add_suffix(file_input);
    row.add_suffix(save_btn);
    row.activatable_widget = save_btn;
    return row;
}

/**
 *
 * @param {Gtk.Button} save_btn the button
 * @param {string} path the file
 */
function update_btn_label(save_btn, path) {
    save_btn.label = fexists(path) ? 'Save' : 'Save & create file';
}

/**
 * Main function for prefereces api
 *
 * @param {Adw.Window} window preferences window
 */
// eslint-disable-next-line no-unused-vars
function fillPreferencesWindow(window) {
    const settings = ExtensionUtils.getSettings();

    const page = new Adw.PreferencesPage();
    const group = new Adw.PreferencesGroup();

    const sources = settings.get_value('sources').recursiveUnpack();
    // avoid mutations problems
    const default_sources = () => {
        return settings.get_value('defaultsources').recursiveUnpack();
    };

    window.add(page);
    page.add(group);
    group.add(create_logfile_row());
    page.add(create_new_source_form(sources, default_sources, page));
    for (let sourcename in sources)
        page.add(create_source_group(sourcename, page, sources));

    // Make sure the window doesn't outlive the settings object
    window._settings = settings;
}
