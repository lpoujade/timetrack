
const Me = imports.misc.extensionUtils.getCurrentExtension();
const {ItemsSource} = Me.imports.source;
const {Soup} = imports.gi;
const ByteArray = imports.byteArray;

const GITLAB_GQL_URL = '/api/graphql';

var GitlabSource = class extends ItemsSource {
    constructor(name, props) {
        super(name, props);
        this.url = this.props.url + GITLAB_GQL_URL;
        this.username = this.props.username;
        this.token = this.props.token;

        this.session = Soup.Session.new();
    }

    load() {
        if (this.items.length === 0) {
            const _doc = JSON.stringify({query: `{issues(assigneeUsernames: ["${this.username}"], state:opened) { nodes { webUrl iid title } } }`});
            let raw_items = null;

            const request = Soup.Message.new('POST', this.url);
            const reqbody = Uint8Array.from(Array.from(_doc).map(letter => letter.charCodeAt(0)));
            request.set_request_body_from_bytes('application/json', reqbody);
            request.request_headers.append('Accept', 'application/json');
            request.request_headers.append('Authorization', `Bearer ${this.token}`);
            try {
                const r = this.session.send_and_read(request, null);
                const rbody = ByteArray.toString(r.get_data());
                raw_items = JSON.parse(rbody);
                this.items = raw_items.data.issues.nodes.map(e => `#${e.iid} ${e.title}`);
            } catch (e) {
                console.log('catched network exception');
            }
        }
    }
};
